Installing SourceTree

Go to https://www.sourcetreeapp.com/ and download the appropriate version for your OS.

Download the App and run it.

Install & License agreement:
Accept the licence agreement and decide if you wish to send data back to Atlassian to improve SourceTree.

Atlassian account:
Log in or create an Atlassian account.

Remotes:
Select the left most box (Bitbucket) and select OAuth in the Authentication drop down menu and click "Continue". This will open your browser and should say "Authentication Successful".

Install tools:
Go back to the installer and wait for the "Tool installation" to complete. Leave the "Configure automatic line ending handling by default" box checked. Click "Continue".

If a window titled "Install global ignore file?" pops up, click "yes".

Starting repository:
Click "Skip Setup".
If a window titled "Load SSH Key?" pops up, click "no".

Clone a repository from your remote Bitbucket account:
1) In the SourceTree App, click the "Clone" tab.
2) Click the blue text "remote account" link.
3) Click the blue text "Clone" link next to "Project1". The "Source Path / URL:" field in the "Clone" tab will be filled automatically.
4) Underneath the first feild is the "Destination Path:" field and is where the files will be saved locally on your machine. Click the "Browse" button next to this field.  Go to your Desktop (or some other easily remembered location) and create a new folder called Bitbucket (or anything else appropirately named...). Go inside the "Bitbucket" folder and create another folder, name this folder the same as the repository you are downloading (Project1, in this example). Click "Select Folder".
5) Click the "Clone" button at the bottom of the window.

Create a new Repository in SourceTree.
1) Create a file called "Bitbucket" on your desktop (if not already done so).
2) In SourceTree, at the main menu (Hint: open a new tab if needed.), click the "Create" tab.
3) Click the "Browse" button and look for the folder you created in step 1 ("Bitbucket").  Add a "\" to the path and then type the name you want the repository to be. Note!  The name must be lowercase, alphanumerical, and may also contain underscores, dashes, or dots.
4) Make sure the file you are trying to push to the new repository is empty and the name doesn't exist  Click the "Create" button.
5) Move any files you want to be into the new local folder.
6) In SourceTree, go to "Local" tab.
7) Double click on the repository you just made.
8) Click the "Stage All" button.
9) Type a commit message/comment in the field bellow your name and email address and click "Commit".
10) Click the "Push" button in the toolbar.
11) Check "Select All" checkbox in the pop-up window and click the "Push" button.